from sklearn import datasets
from datasets.VirusDataset import VirusDataset
import torch
import torchvision
from models.CustomModelFactory import CustomModelFactory
from torch import nn
from torch.utils.data.dataloader import DataLoader
from torch.optim import Optimizer
from torch.utils.data import WeightedRandomSampler
from sklearn.metrics import confusion_matrix, PrecisionRecallDisplay
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sn
from torchvision.datasets import ImageFolder


def train(model, dataloader: DataLoader, optimizer: Optimizer, loss_fn, device: torch.device, epochs=100):
    print("Started traning")
    losses = []
    size = len(dataloader.dataset)
    model.train()
    for epoch in range(epochs):
        print(f'Epoch {epoch}/{epochs}')
        batch_losses = []

        for batch, (X, y) in enumerate(dataloader):
            X, y = X.to(device), y.to(device)

            # Compute prediction error
            pred = model(X)
            loss = loss_fn(pred, y)

            # Backpropagation
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            if batch % 1 == 0:
                loss, current = loss.item(), batch * len(X)
                print(f"loss: {loss}  [{current:>5d}/{size:>5d}]")
                batch_losses.append(loss)
        losses.append(np.mean(batch_losses))

    plt.figure(figsize=(12, 7))
    plt.title('Loss vs Epochs')
    plt.plot(losses)
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.savefig('lossVsEpochs.png')
    print("Finished training")


def test(model, dataloader: DataLoader, loss_fn, device: torch.device, sample_weights):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    model.eval()
    test_loss, correct = 0, 0
    y_pred, y_pred_probabilities, y_true = [], [], []
    with torch.no_grad():
        for X, y in dataloader:
            X, y = X.to(device), y.to(device)
            pred = model(X)

            test_loss += loss_fn(pred, y).item()
            softmax = torch.nn.Softmax(dim=1)

            pred_probabilities = softmax(pred)
            pred_probabilities = torch.max(pred_probabilities, 1)[0]
            pred = torch.max(pred, 1)[1]

            correct += (pred == y).type(torch.float).sum().item()

            y_pred.extend(pred.cpu().numpy())
            y_pred_probabilities.extend(pred_probabilities.cpu().numpy())
            y_true.extend(y.cpu().numpy())
    test_loss /= num_batches
    correct /= size
    print(
        f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss} \n")
    # constant for classes
    classes = ('Benign', 'Malware')

    # Build confusion matrix

    cf_matrix = confusion_matrix(
        y_true, y_pred, sample_weight=sample_weights, normalize='true')

    df_cm = pd.DataFrame(cf_matrix, index=[i for i in classes],
                         columns=[i for i in classes])
    plt.figure(figsize=(12, 7))
    sn.heatmap(df_cm, annot=True)
    plt.title('Confusion Matrix')
    plt.xlabel('True labels')
    plt.ylabel('Predicted label')
    plt.savefig('confusionmatrix.png')

    plt.figure(figsize=(12, 7))
    PrecisionRecallDisplay.from_predictions(y_true, y_pred_probabilities)
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.title('Percision Recall Curve')
    plt.savefig('precisionrecall.png')


def main():
    epochs = 600
    learning_rate = 0.00001

    # Making sample weights for the sampler
    classes_0 = 2341
    classes_1 = 46081
    w0 = (classes_0 + classes_1) / (2 * classes_0)
    w1 = (classes_0 + classes_1) / (2 * classes_1)

    weights = [w0, w1]

    # Load the dataset
    # Make transformations
    transform_label = torchvision.transforms.Compose([
        torchvision.transforms.Lambda(lambda x: 1 if x >= 1 else 0),
    ])

    transform_data = torchvision.transforms.Compose([
        torchvision.transforms.RandomRotation(degrees=(0, 180)),
        torchvision.transforms.RandomHorizontalFlip(p=0.5),
        torchvision.transforms.RandomVerticalFlip(p=0.5),
    ])
    malimage_dataset, malimage_test = prepare_malimage_dataset()

    dataset = VirusDataset('data/VirusMNIST/train.csv', transform_data, transform_label)
    # Uncomment this line to merge with malimage dataset
    #dataset = torch.utils.data.ConcatDataset([dataset, malimage_dataset])

    # Create sampler using the given weights
    sample_weights = [weights[y] for _, y in dataset]
    sampler = WeightedRandomSampler(sample_weights, len(sample_weights), replacement=True)

    # Load VirusMNIST test set
    dataset_test = VirusDataset('data/VirusMNIST/test.csv', None, transform_label, False)
    classes_0 = 175
    classes_1 = 3283
    w0 = (classes_0 + classes_1) / (2 * classes_0)
    w1 = (classes_0 + classes_1) / (2 * classes_1)

    weights = [w0, w1]
    # Create weights for the test
    sample_weights_test = [weights[y] for _, y in dataset_test]

    # Set setting for GPU training
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    train_kwargs = {'batch_size': 32}
    test_kwargs = {'batch_size': 32}
    if torch.cuda.is_available():
        print("Using GPU")
        cuda_kwargs = {'num_workers': 6,
                       'pin_memory': True}
        train_kwargs.update(cuda_kwargs)
        test_kwargs.update(cuda_kwargs)

    dataloader_test = DataLoader(dataset_test, **test_kwargs)
    dataloader = DataLoader(dataset, sampler=sampler, **train_kwargs)

    # Load the model
    model = CustomModelFactory().get_model()
    model = model.to(device)

    loss_fn = nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

    # Train the model
    # train(model, dataloader, optimizer, loss_fn, device, epochs)
    # Save the model
    model_name = f"model{epochs}epchosWithAugFINALllr{learning_rate}.pth"
    # torch.save(model.state_dict(), model_name)
    print(f"Saved PyTorch Model State to {model_name}")

    # Load saved model
    # model.load_state_dict(torch.load(model_name))
    # Evaluate the model
    test(model, dataloader_test, loss_fn, device, sample_weights_test)


def prepare_malimage_dataset():
    # Transformations for MalImage Dataset
    transform_label = torchvision.transforms.Compose([
        torchvision.transforms.Lambda(lambda _: 1),
    ])
    transform_data = torchvision.transforms.Compose([
        torchvision.transforms.Resize(size=(32, 32)),
        torchvision.transforms.ToTensor(),
        torchvision.transforms.Normalize((0.1307,), (0.3081,)),
        torchvision.transforms.Grayscale(num_output_channels=1),
    ])

    dataset = ImageFolder('data/malimage', transform_data, transform_label)
    # Split 80/20 for train and test
    train_size = int(0.8 * len(dataset))
    test_size = len(dataset) - train_size
    dataset, test = torch.utils.data.random_split(
        dataset, [train_size, test_size])

    return dataset, test


if __name__ == '__main__':
    main()
