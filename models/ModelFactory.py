class ModelFactory:

    def __init__(self) -> None:
        pass

    def get_model(self):
        raise NotImplementedError()
