import torch
from .ModelFactory import ModelFactory
import torchvision
from torch import nn
from .FashionMNIST import FashionCNN


class CustomModelFactory(ModelFactory):
    def __init__(self):
        pass

    def get_model(self):
        return FashionCNN()
