import torch
from .ModelFactory import ModelFactory
import torchvision
from torch import nn


class VGG11Factory(ModelFactory):
    def __init__(self):
        pass

    def get_model(self):
        model = torchvision.models.vgg11(pretrained=True)
        flag = 0
        # We want this to happen only for 2 children
        for child in model.children():
            for parameter in child.parameters():
                parameter.requires_grad = False
            flag += 1

        model.classifier.add_module('lastfc', nn.Linear(1000, 500))
        model.classifier.add_module('lastRelu', nn.ReLU())
        # model.classifier.add_module('lastDropout', nn.Dropout(0.2))
        model.classifier.add_module('lastfc2', nn.Linear(500, 1))
        model.classifier.add_module('lastRelu2', nn.ReLU())
        # model.classifier.add_module('lastDropout2', nn.Dropout(0.2))
        model.classifier.add_module('lastSigmoid', nn.Sigmoid())
        return model
