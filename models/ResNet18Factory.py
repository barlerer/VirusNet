from . import ModelFactory
import torch
import torchvision


class ResNet18Factory(ModelFactory):
    def __init__(self):
        pass

    def get_model(self):
        return torchvision.models.resnet18(pretrained=True)
