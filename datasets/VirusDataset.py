import pandas as pd
from torch.utils.data import Dataset
import numpy as np
import torch


class VirusDataset(Dataset):
    def __init__(self, path_file, transforms=None, target_transform=None, train=True) -> None:
        self.data = pd.read_csv(path_file)
        self.transforms = transforms
        self.target_transform = target_transform

    def __getitem__(self, index: int) -> None:
        img_label = torch.tensor(self.data.iloc[index, 0], dtype=torch.float)
        img_data = torch.from_numpy(
            self.data.iloc[index, 1:-1].values.astype(np.float32)).reshape(32, 32)
        img_data = img_data.repeat((1, 1, 1))
        if self.transforms:
            img_data = self.transforms(img_data)
        if self.target_transform:
            img_label = self.target_transform(img_label)
        return img_data/255, img_label

    def __len__(self) -> int:
        return len(self.data)
